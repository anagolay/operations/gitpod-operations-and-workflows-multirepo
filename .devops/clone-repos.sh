#!/usr/bin/env bash

PROJECT_ROOT=$(git rev-parse --show-toplevel)

echo "Update and install system deps:"

sudo apt update && sudo apt install -y direnv

if [ ! -d "$PROJECT_ROOT/op_file" ]; then
	echo "Cloning the Op_File to the root for testing"
	git clone git@gitlab.com:anagolay/operations/op_file.git $PROJECT_ROOT/op_file
fi

if [ ! -d "$PROJECT_ROOT/op_multihash" ]; then
	echo "Cloning the Op-Multihash to the root for testing"
	git clone https://gitlab.com/anagolay/operations/op_multihash.git $PROJECT_ROOT/op_multihash
fi

if [ ! -d "$PROJECT_ROOT/op_cid" ]; then
	echo "Cloning the Op-cid to the root for testing"
	git clone https://gitlab.com/anagolay/operations/op_cid.git $PROJECT_ROOT/op_cid
fi

if [ ! -d "$PROJECT_ROOT/workflow-template" ]; then
	echo "Cloning the Op-cid to the root for testing"
	git clone https://gitlab.com/anagolay/anagolay-workflow-template.git $PROJECT_ROOT/workflow-template
fi
if [ ! -d "$PROJECT_ROOT/an_operation_support" ]; then
	echo "Cloning the Op-cid to the root for testing"
	git clone https://gitlab.com/anagolay/operations/an_operation_support.git $PROJECT_ROOT/operation_support
fi
if [ ! -d "$PROJECT_ROOT/op_collect" ]; then
	echo "Cloning the Op-cid to the root for testing"
	git clone https://gitlab.com/anagolay/operations/op_collect.git $PROJECT_ROOT/op_collect
fi

# install pnpm
echo "Installing pnpm"
npm install -g pnpm
